//
//  User+CoreDataProperties.swift
//  Galeria de Arte
//
//  Created by Marcos Arroyo Gonzalez on 19/01/20.
//  Copyright © 2020 Marcos Arroyo Gonzalez. All rights reserved.
//
//

import Foundation
import CoreData


extension User {

    @nonobjc public class func fetchRequest() -> NSFetchRequest<User> {
        return NSFetchRequest<User>(entityName: "User")
    }

    @NSManaged public var contra: String?
    @NSManaged public var nombre: String?
    @NSManaged public var usuario: String?

}
