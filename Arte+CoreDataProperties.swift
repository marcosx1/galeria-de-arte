//
//  Arte+CoreDataProperties.swift
//  Galeria de Arte
//
//  Created by Marcos Arroyo Gonzalez on 20/01/20.
//  Copyright © 2020 Marcos Arroyo Gonzalez. All rights reserved.
//
//

import Foundation
import CoreData


extension Arte {

    @nonobjc public class func fetchRequest() -> NSFetchRequest<Arte> {
        return NSFetchRequest<Arte>(entityName: "Arte")
    }

    @NSManaged public var anio: String?
    @NSManaged public var autor: String?
    @NSManaged public var descrip: String?
    @NSManaged public var fecharegistro: Date?
    @NSManaged public var nombre: String?
    @NSManaged public var precio: String?
    @NSManaged public var tamanio: String?
    @NSManaged public var ubi: String?
    @NSManaged public var url: String?
    @NSManaged public var id: Int16

}
