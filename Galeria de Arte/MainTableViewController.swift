//
//  MainTableViewController.swift
//  Galeria
//
//  Created by Marcos Arroyo Gonzalez on 17/01/20.
//  Copyright © 2020 Marcos Arroyo Gonzalez. All rights reserved.
//

import UIKit
import CoreData

class MainTableViewController: UITableViewController, UISearchBarDelegate, UISearchResultsUpdating {

    @IBOutlet weak var sbbuscar: UISearchBar!
    var bandera = true
    var listaarte: [Arte] = []
    var auxlistaarte: [Arte] = []
    lazy var refresh: UIRefreshControl = {

        let refreshaux = UIRefreshControl()
        refreshaux.addTarget(self, action: #selector(actualizar), for: UIControl.Event.valueChanged)

        return refreshaux
    }()

    // var context:NSManagedObjectContext!
    override func viewDidLoad() {
        super.viewDidLoad()

        sbbuscar.delegate = self
        self.tableView.addSubview(refresh)
        getDatafromRest()
        self.hideKeyboard()
        //let logout = UIBarButtonItem(title: "Cerrar Sesión", style: .done, target: .none, action: {})

    }

    // MARK: - Table view data source

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
        if bandera {
            return listaarte.count
        } else {
            return auxlistaarte.count
        }
    }


    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cuadrocell", for: indexPath) as? CuadroTableViewCell
        var arte: Arte?
        if bandera {
            arte = listaarte[indexPath.row]
        } else {
            arte = auxlistaarte[indexPath.row]
        }
        cell?.setupArt(arte!)
        return cell!
    }

    @objc func actualizar() {

        self.recargar()
        self.refresh.endRefreshing()

    }

    func recargar() {

        self.tableView.reloadData()

    }

    // MARK: - Search Bar

    func updateSearchResults(for searchController: UISearchController) {

    }

    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {

        self.auxlistaarte = self.listaarte.filter { (elemento: Arte) -> Bool in
            if elemento.nombre!.lowercased().contains(self.sbbuscar.text!.lowercased()) {
                return true
            } else {
                return false
            }
        }

        if (searchText != "") {
            self.bandera = false
            self.recargar()
        } else {
            self.bandera = true
            self.recargar()
        }

    }

    // MARK: - Bar Button

    @IBAction func logout(_ sender: UIBarButtonItem) {

        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let nabcontroler = storyboard.instantiateViewController(identifier: "inicialNC") as? UINavigationController
        nabcontroler?.modalPresentationStyle = .fullScreen
        self.present(nabcontroler!, animated: true, completion: nil)

    }


    // MARK: - Core Data and WS REST
    func getDatafromRest() {

        guard let url = URL(string: "https://maggrupoempresarial.com/mygalery/getarte.php") else {
            return
        }
        var request = URLRequest(url: url)
        request.httpMethod = "POST"
        request.addValue("applicaction/json", forHTTPHeaderField: "Content-Type")
        let session = URLSession.shared
        session.dataTask(with: request) { (data, response, error) in
            if error != nil {
                print(error.debugDescription)
            } else {
                DispatchQueue.main.async {
                    do {
                        let json = try JSONSerialization.jsonObject(with: data!, options: JSONSerialization.ReadingOptions.mutableContainers) as! [[String: Any]]
                        guard let jsonArray = json as? [[String: Any]] else {
                            return
                        }

                        let request = NSFetchRequest<Arte>(entityName: "Arte")
                        request.returnsObjectsAsFaults = false
                        do {
                            let appDelegate = UIApplication.shared.delegate as! AppDelegate
                            let context = appDelegate.persistentContainer.viewContext
                            let result = try context.count(for: request)
                            if result != jsonArray.count {
                                self.deleteData("Arte")
                                print("Actualizando datos......")
                                for arte in jsonArray {
                                    self.saveArt(arte)
                                }
                            } else {
                                self.readDataDC()
                            }
                        } catch {
                            print("error")
                        }
                    } catch {
                        print("consumiendo WS REST\(error)")
                    }
                }
            }
        }.resume()
    }

    func readDataDC() {

        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        let context = appDelegate.persistentContainer.viewContext
        let peticion = NSFetchRequest<Arte>(entityName: "Arte")
        do {
            let cuadros = try context.fetch(peticion)
            listaarte = cuadros
            self.recargar()
        } catch let error as NSError {
            print(error.userInfo)
        }

    }


    func saveArt(_ art: [String: Any]) {

        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        let context = appDelegate.persistentContainer.viewContext
        let entity = NSEntityDescription.entity(forEntityName: "Arte", in: context)!
        let newArt = Arte(entity: entity, insertInto: context)
        newArt.anio = (art["anio"] as! String)
        newArt.autor = (art["autor"] as! String)
        newArt.descrip = (art["descripcion"] as! String)
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
        let date = dateFormatter.date(from: art["created"] as! String)
        newArt.fecharegistro = date!
        newArt.nombre = (art["nombre_arte"] as! String)
        newArt.precio = (art["precio"] as! String)
        newArt.tamanio = (art["tamaño"] as! String)
        newArt.ubi = (art["localizacion"] as! String)
        newArt.id = (Int16(art["id"] as! String)!)
        let nameart = (art["images"] as! String)
        let url = nameart.replacingOccurrences(of: " ", with: "%20")
        newArt.url = "https://galery.maggrupoempresarial.com/img/\(url)"
        do {
            try context.save()
            readDataDC()

        } catch let error as NSError {
            print(error.userInfo)
        }

    }

    func deleteData(_ entity: String) {

        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        let context = appDelegate.persistentContainer.viewContext
        let fetchRequest = NSFetchRequest<NSFetchRequestResult>(entityName: entity)
        fetchRequest.returnsObjectsAsFaults = false
        do {
            let results = try context.fetch(fetchRequest)
            for managedObject in results {
                let managedObjectData: NSManagedObject = managedObject as! NSManagedObject
                context.delete(managedObjectData)
                try context.save()
                print("Borrando....")
            }
        } catch let error as NSError {
            print("Deleted all my data in myEntity error : \(error) \(error.userInfo)")
        }

    }


    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {

        if let DetallesVC = segue.destination as? ArteVerViewController {
            if let selectArt = sender as? Arte {
                DetallesVC.art = selectArt
            }
        }

    }

    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {

        if bandera {
            performSegue(withIdentifier: "segueArteView", sender: listaarte[indexPath.row])
        } else {
            performSegue(withIdentifier: "segueArteView", sender: auxlistaarte[indexPath.row])
        }

//        let storyboard = UIStoryboard(name: "Main", bundle: nil)
//        let controlador = storyboard.instantiateViewController(identifier: "detailsVC") as? ArteVerViewController
//        controlador!.art = listaarte[indexPath.row]
//        self.present(controlador!, animated: true, completion: nil)

    }

    func hideKeyboard() {
        let tap: UITapGestureRecognizer = UITapGestureRecognizer(
                target: self,
                action: #selector(self.dismissKeyboard))

        tap.cancelsTouchesInView = false
        view.addGestureRecognizer(tap)
    }

    @objc func dismissKeyboard() {
        view.endEditing(true)
    }


}
