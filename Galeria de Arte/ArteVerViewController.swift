//
//  ArteVerViewController.swift
//  Galeria de Arte
//
//  Created by Marcos Arroyo Gonzalez on 20/01/20.
//  Copyright © 2020 Marcos Arroyo Gonzalez. All rights reserved.
//

import UIKit
import Kingfisher

class ArteVerViewController: UIViewController {

    @IBOutlet weak var lblnombre: UILabel!
    @IBOutlet weak var imgArte: UIImageView!
    @IBOutlet weak var lblautor: UILabel!
    @IBOutlet weak var lblprecio: UILabel!
    @IBOutlet weak var lbldescrip: UILabel!
    @IBOutlet weak var lblfecha: UILabel!

    var art: Arte?

    override func viewDidLoad() {
        super.viewDidLoad()

        lblnombre.text = String(art!.nombre ?? " ")
        lblautor.text = "Autor: \(art?.autor ?? " ")"
        lblprecio.text = "Precio: \(art!.precio ?? " ")"
        lbldescrip.text = " Descripcion: \n \(art!.descrip ?? " ")"
        let dateFormatter = DateFormatter()
        dateFormatter.dateStyle = .full
        dateFormatter.locale = Locale.init(identifier: "es_MX")
        let fecha = dateFormatter.string(from: art!.fecharegistro ?? Date())
        lblfecha.text = "Fecha: \(fecha)"
        guard let url = URL(string: art!.url!) else {
            return
        }
        DispatchQueue.global(qos: .userInitiated).async {
            let imgData: NSData = NSData(contentsOf: url)!
            DispatchQueue.main.async {
                let image = UIImage(data: imgData as Data)
                self.imgArte.image = image
            }
        }

    }

    func configImg(_ urlimg: URL) {
        let process = RoundCornerImageProcessor(cornerRadius: 15)
        imgArte.kf.indicatorType = .activity
        imgArte.kf.setImage(
                with: urlimg,
                options: [
                    .processor(process),
                    .transition(.fade(1)),
                    .cacheOriginalImage
                ])
    }


    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
