//
//  ViewController.swift
//  Galeria de Arte
//
//  Created by Marcos Arroyo Gonzalez on 18/01/20.
//  Copyright © 2020 Marcos Arroyo Gonzalez. All rights reserved.
//

import UIKit
import LocalAuthentication
import CoreData

class ViewController: UIViewController {

    @IBOutlet weak var txtusuario: UITextField!
    @IBOutlet weak var txtcontra: UITextField!
    @IBOutlet weak var btnBio: UIButton!

    let objManagedContext = ManagedContextViewController()
    let context = LAContext()
    var strAlert = String()
    var error: NSError?

    //var managedContext:NSManagedObjectContext!
    override func viewDidLoad() {
        super.viewDidLoad()
        checkBiometrics()
        guardarUsuariosPlist()
        self.hideKeyboard()
    }

    // MARK: - Navigation

    func goToNext() {

        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let nabcontroler = storyboard.instantiateViewController(identifier: "nabmaingaleria") as? UINavigationController
        nabcontroler?.modalPresentationStyle = .fullScreen
        //controlador!.context = managedContext
        self.present(nabcontroler!, animated: true, completion: nil)

    }

    // MARK: - Login Methods

    @IBAction func loginNormal(_ sender: UIButton) {

        guard let usuario = txtusuario.text, let pass = txtcontra.text else {
            self.notifyUser("Necesita ingresar datos para poder ingresar")
            return
        }
        self.loginCheckDC(usuario, pass)

    }

    @IBAction func loginBio(_ sender: UIButton) {

        if context.canEvaluatePolicy(LAPolicy.deviceOwnerAuthenticationWithBiometrics, error: &error) {
            context.evaluatePolicy(.deviceOwnerAuthenticationWithBiometrics, localizedReason: self.strAlert) { (success, error) in
                DispatchQueue.main.async {
                    if success {
                        self.goToNext()
                    } else {
                        if let err = error {
                            let strMessage = self.errorMessager(errorcode: err._code)
                            self.notifyUser(strMessage)
                        }
                    }
                }
            }
        } else {
            if let err = error {
                let strMessage = self.errorMessager(errorcode: err._code)
                self.notifyUser(strMessage)
            }
        }

    }

    // MARK: - Core Data

    func guardarUsuariosPlist() {

        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        let context = appDelegate.persistentContainer.viewContext
        let request = NSFetchRequest<User>(entityName: "User")
        request.returnsObjectsAsFaults = false
        do {
            let result = try context.count(for: request)
            if result < 1 {
                print("guardando....")
                let usuariosplist = Bundle.main.path(forResource: "DataUsers", ofType: "plist")
                let arrayUsers = NSArray(contentsOfFile: usuariosplist!)!
                for user in arrayUsers {
                    let entity = NSEntityDescription.entity(forEntityName: "User", in: context)!
                    let userentity = User(entity: entity, insertInto: context)
                    let diccionaryuserts = user as! [String: AnyObject]
                    let nameUser = diccionaryuserts["name"] as? String
                    let userUser = diccionaryuserts["user"] as? String
                    let passwordUser = diccionaryuserts["password"] as? String
                    userentity.nombre = nameUser
                    userentity.usuario = userUser
                    userentity.contra = passwordUser
                }
                do {
                    try context.save()
                } catch let error as NSError {
                    print(error.userInfo)
                }
            } else {
                print("ya hay datos guardados")
            }
        } catch let error as NSError {
            print(error.userInfo)
        }

    }

    func loginCheckDC(_ user: String, _ pass: String) {

        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        let context = appDelegate.persistentContainer.viewContext
        let request = NSFetchRequest<User>(entityName: "User")
        request.predicate = NSPredicate(format: "usuario = %@ AND contra = %@", argumentArray: [user, pass])
        request.returnsObjectsAsFaults = false
        do {
            let result = try context.count(for: request)
            if result >= 1 {
                self.goToNext()
            } else {
                self.notifyUser("Los datos son incorrectos, intente de nuevo")
            }
        } catch let error as NSError {
            print(error.userInfo)
        }

    }

    // MARK: - Biometric Button
    func checkBiometrics() {

        if context.canEvaluatePolicy(.deviceOwnerAuthenticationWithBiometrics, error: &error) {
            switch context.biometryType {
            case .faceID:
                btnBio.setImage(UIImage(named: "faceid"), for: .normal)
                self.strAlert = "Face ID"
                break
            case .touchID:
                btnBio.setImage(UIImage(named: "touchid"), for: .normal)
                self.strAlert = "Touch ID"
                break
            default:
                break
            }
        } else {
            if let err = error {
                let stralert = self.errorMessager(errorcode: err._code)
                self.notifyUser(stralert)
            }
        }

    }

    // MARK: - Alerts User

    func notifyUser(_ err: String?) {

        let alert = UIAlertController(title: "¡Error!", message: err, preferredStyle: .alert)
        let cancelAction = UIAlertAction(title: "OK", style: .cancel, handler: nil)
        alert.addAction(cancelAction)
        self.present(alert, animated: true, completion: nil)

    }


    func errorMessager(errorcode: Int) -> String {

        var strMessage = ""
        switch errorcode {
        case LAError.authenticationFailed.rawValue:

            strMessage = "fallo la autentificacion"
            break

        case LAError.userCancel.rawValue:

            strMessage = "el usuario cancelo la identificacion"
            break
        case LAError.userFallback.rawValue:

            strMessage = "fallo en la aplicacion"
            break
        case LAError.systemCancel.rawValue:

            strMessage = "El sistema cerro la identificacion"
            break

        case LAError.passcodeNotSet.rawValue:

            strMessage = "No se completo la identificacion"
            break

        case LAError.biometryNotAvailable.rawValue:

            strMessage = "Sensor biometrico no disponible"
            break

        case LAError.appCancel.rawValue:

            strMessage = "La aplicacion cerro la identificacion"
            break

        case LAError.invalidContext.rawValue:

            strMessage = "Contexto invalido"
            break

        default:

            strMessage = "Error no conocido"
            break
        }

        return strMessage
    }

    func hideKeyboard() {
        let tap: UITapGestureRecognizer = UITapGestureRecognizer(
                target: self,
                action: #selector(self.dismissKeyboard))

        tap.cancelsTouchesInView = false
        view.addGestureRecognizer(tap)
    }

    @objc func dismissKeyboard() {
        view.endEditing(true)
    }


}

extension ViewController: sendContext {
    
    func getConext(managedContext: NSManagedObjectContext) {
        //self.managedContext = managedContext
    }


}
