//
//  ManagedContextViewController.swift
//  Galeria de Arte
//
//  Created by Marcos Arroyo Gonzalez on 20/01/20.
//  Copyright © 2020 Marcos Arroyo Gonzalez. All rights reserved.
//

import UIKit
import CoreData

protocol sendContext {
    func getConext(managedContext: NSManagedObjectContext)

}

class ManagedContextViewController: UINavigationController {

    var managedContext: NSManagedObjectContext!
    var delegateContext: sendContext?

    override func viewDidLoad() {
        super.viewDidLoad()
        delegateContext?.getConext(managedContext: managedContext)
//        print("segue en curso")
//        let story = UIStoryboard(name: "Main", bundle: nil)
//        let view = story.instantiateViewController(identifier: "loginView") as? ViewController
//        view!.managedContext = managedContext
        // Do any additional setup after loading the view.
    }

    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {

    }

}
