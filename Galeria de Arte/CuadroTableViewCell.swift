//
//  CuadroTableViewCell.swift
//  Galeria
//
//  Created by Marcos Arroyo Gonzalez on 17/01/20.
//  Copyright © 2020 Marcos Arroyo Gonzalez. All rights reserved.
//

import UIKit
import Kingfisher

class CuadroTableViewCell: UITableViewCell {

    @IBOutlet weak var imgCuadro: UIImageView!
    @IBOutlet weak var lblnombreCuadro: UILabel!
    @IBOutlet weak var lblautorCuadro: UILabel!
    @IBOutlet weak var lbldescripCuadro: UILabel!


    func setupArt(_ arte: Arte) {

        self.lblnombreCuadro.text = arte.nombre!
        self.lblautorCuadro.text = "Autor: \(arte.autor!)"
        let dateFormatter = DateFormatter()
        dateFormatter.dateStyle = .full
        dateFormatter.locale = Locale.init(identifier: "es_MX")
        let fecha = dateFormatter.string(from: arte.fecharegistro!)
        lbldescripCuadro.text = fecha
        guard let url = URL(string: arte.url!) else {
            return
        }
        configImg(url)


    }

    func configImg(_ urlimg: URL) {
        let process = RoundCornerImageProcessor(cornerRadius: 15)
        imgCuadro.kf.indicatorType = .activity
        imgCuadro.kf.setImage(
                with: urlimg,
                options: [
                    .processor(process),
                    .transition(.fade(1)),
                    .cacheOriginalImage
                ])
    }

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
